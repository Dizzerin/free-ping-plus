FROM python:alpine

RUN mkdir -p /project

WORKDIR /project

COPY src .

RUN apk update && apk add --no-cache \
    bash \
    vim \
    bash-completion \
    postgresql-dev

RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "./main.py" ]